    </div>

    <!-- Footer -->
    <footer>
		<!-- <div class="container"> -->
			<div class="footer-left">
				<div class="logo-footer">
					<?= get_logo() ?>
				</div>
				<p>
				Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam tincidunt velit eu laoreet egestas. Donec porttitor unt venenatis. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam tincidunt velit eu laoreet egestas. Donec porttitor unt venenatis. Lorem ipsum dolor sit amet, consectetur adipiscing
				</p>

				<div class="socias-footer">
					<a href="https://www.facebook.com/arealocal?fref=ts" title="Curta nossa página" alt="facebook" class="facebook" target="_blank"><?= svg('facebook_icon'); ?></a>
					<a href="https://instagram.com/arealocal" title="Nossas fotos no Instagram" alt="instagram" class="instagram" target="_blank"><?= svg('instagram_icon'); ?></a>
					<a href="https://www.linkedin.com/company/2592225?trk=vsrp_companies_res_name&trkInfo=VSRPsearchId%3A4053558421424728142768%2CVSRPtargetId%3A2592225%2CVSRPcmpt%3Aprimary" title="Conecte-se a nós" alt="linkedin" class="linkedin" target="_blank"><?= svg('linkedin_icon'); ?></a>
					<a href="https://www.youtube.com/user/arealocal01" title="Nosso canal do Youtube" alt="youtube" class="youtube" target="_blank"><?= svg('youtube_icon'); ?></a>
				</div>
			</div>
			<div class="footer-right">
				<div class="contato-local">
					<div>
						<div><?= svg('support_icon'); ?></div>
						<h4>Contato</h4>
						<p><a href="tel:+554735219850">47 3521-9850</a></p>
						<p><a href="mailto:contato@arealocal.com.br">contato@arealocal.com.br</a></p>
					</div>
					<div>
						<div><?= svg('location_icon'); ?></div>
						<h4>Localização</h4>
						<p><a href="https://goo.gl/maps/KAa37QhsHEhfJLeJA" target="_blank">Rua Antonio Karam, 31 - Sumaré, Rio do Sul - Santa Catarina - 89.165-543</a></p>
						<p>Venha nos fazer uma visita e tomar um café! Aqui não possui Área Azul ;)</p>
					</div>
				</div>
				<div class="footer-newsletter">
					<p>Assine nossa <strong>Newsletter</strong> e fique <br>por dentro das novidades. </p>
					<form action="#" id="form-newsletter">
						<input type="text" name="nome" placeholder="Nome" autocomplete="off">
						<input type="email" name="email" placeholder="seuemail@email.com" autocomplete="off">
						<button type="submit"><i class="fas fa-paper-plane"></i></button>
					</form>
				</div>
			</div>
		<!-- </div> -->
	</footer>

    <!-- Scripts -->
    <script>
        /**
         * @description global JS variables
         */
        window.alUrl = {
            templateUrl: '<?= addslashes(get_bloginfo('template_url')) ?>',
            homeUrl: '<?= addslashes(home_url()) ?>'
        }
	    window.apiUrl = `${window.alUrl.homeUrl}/wp-json/api`
    </script>
    <script async type="text/javascript" src="<?= get_template_directory_uri() ?>/public/app.js"
    >
    </script>
    <?php wp_footer() ?>
</body>
</html>
