/**
 * Modules
 */
const mix = require('laravel-mix')

/**
 * Project proxy
 */
const site = 'your-project-directory'
const proxy = `http://localhost/${site}`

mix
	/**
	 * SCSS/SASS transpiler
	 */
	.sass('assets/scss/index.scss', 'style.css')
	/**
	 * CSS
	 */
	.styles(
	  [
			'assets/css/admin.css',
			'assets/css/libs/*.css'
	  ],
	  'public/libs.css'
	)
	/**
	 * JS transpiler
	 */
	.scripts(
		[
			'assets/js/libs/jquery-3.4.1.min.js',
			'assets/js/libs/axios.min.js',
			'assets/js/libs/jquery.mask.min.js',
			'assets/js/libs/jquery.validate.min.js',
			'assets/js/libs/tiny-slider.js',
			'assets/js/libs/aos.js',
			'assets/js/functions.js',
		],
		'public/app.js'
	)
	/**
	 * JS watcher
	 */
	.js(['assets/js/app.js'], 'public/app.js')
	/**
	 * Configs
	 */
	.disableNotifications()
	.options({ processCssUrls: false })
	.browserSync({ proxy })
	.webpackConfig({
		module: {
			rules: [
				{
					test: /\.js?$/,
					use: [
						{
							loader: 'babel-loader',
							options: mix.config.babel(),
						},
					],
				},
			],
		},
	})
