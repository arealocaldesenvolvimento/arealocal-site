<?php

/* Helpers
========================================================================== */
// include_once get_theme_file_path('app/helpers/...')

/* App
========================================================================== */
include_once get_theme_file_path('app/config.php');
include_once get_theme_file_path('app/scripts/post-types.php');

/* WP REST
========================================================================== */
include_once get_theme_file_path('app/routes.php');

/* Actions & Filters
========================================================================== */
add_action('show_admin_bar', '__return_false');
add_filter('widget_text', 'do_shortcode');
add_filter('retrieve_password_message', 'reset_password_message', null, 2);
remove_action('wp_head', 'wp_generator');

/* Theme functions
========================================================================== */
/**
 * @return string
 */
function wp_mail_return_texthtml(): string
{
	return 'text/html';
}
add_filter('wp_mail_content_type', 'wp_mail_return_texthtml');

/**
 * Returns the full path of the image
 *
 * @param string $file nome do arquivo
 * @return string caminho completo do arquivo
 */
function get_image_url(string $file = ''): string
{
	return get_template_directory_uri().'/assets/images/'.$file;
}

/**
 * Print the full path of the image
 *
 * @param string $file nome do arquivo
 */
function image_url(string $file = ''): void
{
	echo get_image_url($file);
}

/**
 * Print the image of the logo, if it is the home place a wrap of h1
 */
function get_logo(string $img = 'logo.png'): void
{
	$blogName = get_bloginfo('name');

	$tag = '<a href="%s" title="%s - %s" id="header-logo"><img src="%s" alt="%s"></a>';
	$tag = ($tag)?"<h1 id='wrap-logo'>{$tag}</h1>":'';
	$tag = sprintf($tag, home_url('/'), $blogName, get_bloginfo('description'), get_image_url($img), $blogName);

	echo $tag;
}

/**
 * Change text at the bottom of the panel
 */
function change_panel_footer_text(): void
{
	echo '&copy; <a href="http://www.arealocal.com.br/" target="_blank">&Aacute;rea Local</a> - Sites - E-commerce';
}
add_filter('admin_footer_text', 'change_panel_footer_text');

/**
 * Change the login form logo
 */
function change_login_form_logo(): void
{
	echo '<style>.login h1 a{background-image:url('.get_image_url('logo-login-form.png').')!important;}</style>';
}
add_action('login_enqueue_scripts', 'change_login_form_logo');

/**
 * Change the login form logo url
 *
 * @return string
 */
function login_form_logo_url(): string
{
	return get_home_url();
}
add_filter('login_headerurl', 'login_form_logo_url');

/**
 * Standardizes login error message, so as not to show when the user exists
 *
 * @return string
 */
function wrong_login(): string
{
	return '<b>ERRO</b>: Usuário ou senha incorretos.';
}
add_filter('login_errors', 'wrong_login');

/**
 * Change title of the login form logo
 *
 * @return string
 */
function login_form_logo_url_title(): string
{
	return get_bloginfo('name') . ' - Desenvolvido por Área Local';
}
add_filter('login_headertext', 'login_form_logo_url_title');

/**
 * Adds main navigation, html5 support and post thumbnail
 */
function al_setup()
{
	register_nav_menus(['principal' => 'Navegação Principal']);
	add_theme_support('post-thumbnails');
	add_theme_support('html5', ['comment-list', 'comment-form', 'search-form', 'gallery', 'caption']);
}
add_action('after_setup_theme', 'al_setup');

/**
 * Add home in the menu
 *
 * @param array $args
 * @return array
 */
function show_home_menu(array $args): array
{
	$args['show_home'] = true;
	return $args;
}
add_filter('wp_page_menu_args', 'show_home_menu');

/**
 * Records widget areas
 */
function add_widget_areas(): void
{
	register_sidebar([
		'name'          => 'Área de Widget Primária',
		'id'            => 'area-widget-primaria',
		'description'   => 'Área de Widget Primária',
		'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
		'after_widget'  => '</li>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	]);

	register_sidebar([
		'name'          => 'Área de Widget Secundária',
		'id'            => 'area-widget-secundaria',
		'description'   => 'Área de Widget Secundária',
		'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
		'after_widget'  => '</li>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	]);
}
add_action('widgets_init', 'add_widget_areas');

/**
 * Disables wordpress emojis
 */
function disable_wp_emojicons(): void
{
	remove_action('admin_print_styles', 'print_emoji_styles');
	remove_action('wp_head', 'print_emoji_detection_script', 7);
	remove_action('admin_print_scripts', 'print_emoji_detection_script');
	remove_action('wp_print_styles', 'print_emoji_styles');
	remove_filter('wp_mail', 'wp_staticize_emoji_for_email');
	remove_filter('the_content_feed', 'wp_staticize_emoji');
	remove_filter('comment_text_rss', 'wp_staticize_emoji');
}
add_action('init', 'disable_wp_emojicons');

/**
 * Corrects accent file upload bug
 *
 * @param string $filename string nome do arquivo original
 * @return string nome do arquivo higienizado
 */
function sanitize_filename(string $filename): string
{
	$ext       = explode('.', $filename);
	$ext       = end($ext);
	$sanitized = preg_replace('/[^a-zA-Z0-9-_.]/', '', substr($filename, 0, -(strlen($ext)+1)));
	$sanitized = str_replace('.', '-', $sanitized);

	if (function_exists('sanitize_title')) {
		$sanitized = sanitize_title($sanitized);
	}

	return strtolower($sanitized.'.'.$ext);
}
add_filter('sanitize_file_name', 'sanitize_filename', 10);

/**
 * Limit excerpt to a number of characters
 *
 * @param string $excerpt
 * @param int $length
 * @return string
 */
function custom_short_excerpt(string $excerpt, int $length = 150){
    $text = substr($excerpt, 0, $length);

	if (strlen($excerpt) > $length)
        $text .= '...';

	return $text;
}
add_filter('the_excerpt', 'custom_short_excerpt');

/**
 * Change excerpt tag
 *
 * @return string
 */
function change_excerpt_more(): string
{
	return '<a title="'.get_the_title().'" href="'.get_permalink().'" class="more-link">Ler mais</a>';
}
add_filter('excerpt_more', 'change_excerpt_more');

/**
 * Add admin.js and admin.css script to the admin screen
 */
function adminScriptsStyles(): void
{
	wp_enqueue_style('admin-styles', get_bloginfo('template_url').'/assets/css/admin.css');
	wp_enqueue_script('admin-functions', get_bloginfo('template_url').'/assets/js/admin.js');
}
add_action('admin_enqueue_scripts', 'adminScriptsStyles');

/**
 * Render Área Local help panel
 */
function custom_dashboard_help(): void
{
	echo '
		<p>
			Bem-vindo ao tema Área Local! precisa de ajuda?</br> Contate o suporte
			<a target="_blank" href="https://suporte-arealocal.tomticket.com/">aqui!</a>
		</p>
		<h2>Contato</h2>
		<p>Telefone/Whastapp: <a target="_blank" href="https://wa.me/554735219850"><b>(47) 3521-9850</b></a></p>
		<p>E-mail:
			<a target="_blank" href="mailto:contato@arealocal.com.br"><b>contato@arealocal.com.br</b></a>
		</p>
	';
}

/**
 * Enables service area on the Wordpress dashboard
 */
function my_custom_dashboard_widgets(): void
{
	wp_add_dashboard_widget('atendimento-arealocal', 'Atendimento Área Local', 'custom_dashboard_help');
}
add_action('wp_dashboard_setup', 'my_custom_dashboard_widgets');

/**
 * Brings the thumbnail, post image or standard image
 */
//Traz a thumbnail, imagem do post ou img padrão
function get_thumbnail_url($post_id, $size){

    if( !isset($post_id)) {
        $post_id = get_the_ID();
    }
    if( has_post_thumbnail( $post_id ) ) {
		$post_thumbnail_url = get_the_post_thumbnail_url($post_thumbnail_id, $size);
    } else {
        $args = array(
            'numberposts' => 1,
            'order' => 'ASC',
            'post_mime_type' => 'image',
            'post_parent' => $post_id,
            'post_status' => null,
            'post_type' => 'attachment',
        );
		$post_thumbnail_url = get_attached_media('', $post_id);

        if (empty($post_thumbnail_url)) {
            $post_thumbnail_url = get_image_url('default.png');
        }
	}
	if(!$post_thumbnail_url){
		$post_thumbnail_url = get_image_url('default.png');
	}
    return $post_thumbnail_url;
}

/**
 * Render pagination links
 *
 * @param WP_Query $query
 * @param string $url
 * @return void
 */
function paginationLinks(WP_Query $query, string $url = ''): void
{
	$big = 999999999;
	$url = empty($url) ? str_replace($big, '%#%', esc_url(get_pagenum_link($big))):$url.'page/%#%/';

	$imgUrl = get_image_url('flecha-paginacao.svg');

	if ($query->max_num_pages > 1) {
		$current_page = max(1, get_query_var('paged'));

		echo paginate_links([
			'base'      => $url,
			'show_all'  => false,
			'format'    => '?paged=%#%',
			'current'   => $current_page,
			'total'     => $query->max_num_pages,
			'next_text' => __('<i class="fas fa-arrow-right"></i>'),
        	'prev_text' => __('<i class="fas fa-arrow-left"></i>'),
		]);
	}
}

/**
 * Adequate pagination for single-post and archive-post
 *
 * @param WP_Query $query
 * @return void
 */
function custom_posts_per_page(WP_Query $query): void
{
	global $pagenow;

	if ($query->is_archive('post') && $pagenow !== 'edit.php')
		set_query_var('posts_per_page', 1);
}
add_action('pre_get_posts', 'custom_posts_per_page');

/**
 * Change some archive page title
 *
 * @param string $title
 * @return string
 */
function customArchiveTitles(string $title): string
{
	if (is_post_type_archive('some_archive'))
		return 'Some Archive';

	return $title;
}
add_filter('wp_title', 'customArchiveTitles');
add_filter('get_the_archive_title', 'customArchiveTitles');

/**
 * Custom actions before template rendering
 *
 * @param string $template
 * @return string
 */
function preTemplateAction(string $template): string
{
	return $template;
}
add_filter('template_include', 'preTemplateAction');

/**
 * Change admin panel color scheme
 */
wp_admin_css_color(
    'area-structure-4',
    __('Área Structure 4'),
    get_bloginfo('template_url') . '/assets/css/al-admin-color-scheme.min.css',
    [
        '#222',
        '#e0e047',
        '#00244c',
        '#19539a'
    ],
    [
        'base' => '#e5f8ff',
        'focus' => '#fff',
        'current' => '#fff'
    ]
);

/**
 * Set "area-structure-4" as default color scheme
 *
 * @param int $userId
 * @return void
 */
function setDefaultAdminColor(int $userId): void
{
    wp_update_user([
        'ID' => $userId,
        'admin_color' => 'area-structure-4'
    ]);
}
add_action('user_register', 'setDefaultAdminColor');

/*
* Redireciona um Catedory page ou Taxonomy page para uma archive-page ou page
*/
function template_redirect($template) {
    if (is_tax('categoria-produto')) {
        $template = get_query_template('archive-produtos');
    }else if(is_category()){
        $template = get_query_template('page-blog');
    }
    return $template;
}
add_filter('template_include', 'template_redirect');

/*
Provisório
retorna o html do svg
*/
function svg($key){
	$svg_content = array(
		'facebook_icon' =>
			'<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svgjs="http://svgjs.com/svgjs" version="1.1" width="512" height="512" x="0" y="0" viewBox="0 0 155.139 155.139" style="enable-background:new 0 0 512 512" xml:space="preserve" class=""><g><g xmlns="http://www.w3.org/2000/svg"><path id="f_1_" style="" d="M89.584,155.139V84.378h23.742l3.562-27.585H89.584V39.184   c0-7.984,2.208-13.425,13.67-13.425l14.595-0.006V1.08C115.325,0.752,106.661,0,96.577,0C75.52,0,61.104,12.853,61.104,36.452   v20.341H37.29v27.585h23.814v70.761H89.584z" fill="#747373" data-original="#010002" class=""/></g><g xmlns="http://www.w3.org/2000/svg"></g><g xmlns="http://www.w3.org/2000/svg"></g><g xmlns="http://www.w3.org/2000/svg"></g><g xmlns="http://www.w3.org/2000/svg"></g><g xmlns="http://www.w3.org/2000/svg"></g><g xmlns="http://www.w3.org/2000/svg"></g><g xmlns="http://www.w3.org/2000/svg"></g><g xmlns="http://www.w3.org/2000/svg"></g><g xmlns="http://www.w3.org/2000/svg"></g><g xmlns="http://www.w3.org/2000/svg"></g><g xmlns="http://www.w3.org/2000/svg"></g><g xmlns="http://www.w3.org/2000/svg"></g><g xmlns="http://www.w3.org/2000/svg"></g><g xmlns="http://www.w3.org/2000/svg"></g><g xmlns="http://www.w3.org/2000/svg"></g></g></svg>',

		'instagram_icon' =>
			'<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svgjs="http://svgjs.com/svgjs" version="1.1" width="512" height="512" x="0" y="0" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512" xml:space="preserve" class=""><g>
			<g xmlns="http://www.w3.org/2000/svg"><g><path d="M363.273,0H148.728C66.719,0,0,66.719,0,148.728v214.544C0,445.281,66.719,512,148.728,512h214.544    C445.281,512,512,445.281,512,363.273V148.728C512,66.719,445.281,0,363.273,0z M472,363.272C472,423.225,423.225,472,363.273,472    H148.728C88.775,472,40,423.225,40,363.273V148.728C40,88.775,88.775,40,148.728,40h214.544C423.225,40,472,88.775,472,148.728    V363.272z" fill="#747373" data-original="#000000" style="" class=""/></g></g><g xmlns="http://www.w3.org/2000/svg"><g><path d="M256,118c-76.094,0-138,61.906-138,138s61.906,138,138,138s138-61.906,138-138S332.094,118,256,118z M256,354    c-54.037,0-98-43.963-98-98s43.963-98,98-98s98,43.963,98,98S310.037,354,256,354z" fill="#747373" data-original="#000000" style="" class=""/></g>
			</g><g xmlns="http://www.w3.org/2000/svg"><g><circle cx="396" cy="116" r="20" fill="#747373" data-original="#000000" style="" class=""/></g></g>
			<g xmlns="http://www.w3.org/2000/svg"></g><g xmlns="http://www.w3.org/2000/svg"></g><g xmlns="http://www.w3.org/2000/svg"></g><g xmlns="http://www.w3.org/2000/svg"></g><g xmlns="http://www.w3.org/2000/svg"></g><g xmlns="http://www.w3.org/2000/svg"></g><g xmlns="http://www.w3.org/2000/svg"></g><g xmlns="http://www.w3.org/2000/svg"></g><g xmlns="http://www.w3.org/2000/svg"></g><g xmlns="http://www.w3.org/2000/svg"></g><g xmlns="http://www.w3.org/2000/svg"></g><g xmlns="http://www.w3.org/2000/svg"></g><g xmlns="http://www.w3.org/2000/svg"></g><g xmlns="http://www.w3.org/2000/svg"></g><g xmlns="http://www.w3.org/2000/svg"></g></g></svg>',
		'linkedin_icon' =>
			'<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svgjs="http://svgjs.com/svgjs" version="1.1" width="512" height="512" x="0" y="0" viewBox="0 0 682.66669 682" style="enable-background:new 0 0 512 512" xml:space="preserve" class=""><g><path xmlns="http://www.w3.org/2000/svg" d="m77.613281-.667969c-46.929687 0-77.613281 30.816407-77.613281 71.320313 0 39.609375 29.769531 71.304687 75.8125 71.304687h.890625c47.847656 0 77.625-31.695312 77.625-71.304687-.894531-40.503906-29.777344-71.320313-76.714844-71.320313zm0 0" fill="#747373" data-original="#000000" style="" class=""/><path xmlns="http://www.w3.org/2000/svg" d="m8.109375 198.3125h137.195313v412.757812h-137.195313zm0 0" fill="#747373" data-original="#000000" style="" class=""/><path xmlns="http://www.w3.org/2000/svg" d="m482.054688 188.625c-74.011719 0-123.640626 69.546875-123.640626 69.546875v-59.859375h-137.199218v412.757812h137.191406v-230.5c0-12.339843.894531-24.660156 4.519531-33.484374 9.917969-24.640626 32.488281-50.167969 70.390625-50.167969 49.644532 0 69.5 37.851562 69.5 93.339843v220.8125h137.183594v-236.667968c0-126.78125-67.6875-185.777344-157.945312-185.777344zm0 0" fill="#747373" data-original="#000000" style="" class=""/></g></svg>',
		'youtube_icon' =>
			'<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svgjs="http://svgjs.com/svgjs" version="1.1" width="512" height="512" x="0" y="0" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512" xml:space="preserve" class=""><g>
			<g xmlns="http://www.w3.org/2000/svg"><g><path d="M490.24,113.92c-13.888-24.704-28.96-29.248-59.648-30.976C399.936,80.864,322.848,80,256.064,80    c-66.912,0-144.032,0.864-174.656,2.912c-30.624,1.76-45.728,6.272-59.744,31.008C7.36,138.592,0,181.088,0,255.904    C0,255.968,0,256,0,256c0,0.064,0,0.096,0,0.096v0.064c0,74.496,7.36,117.312,21.664,141.728    c14.016,24.704,29.088,29.184,59.712,31.264C112.032,430.944,189.152,432,256.064,432c66.784,0,143.872-1.056,174.56-2.816    c30.688-2.08,45.76-6.56,59.648-31.264C504.704,373.504,512,330.688,512,256.192c0,0,0-0.096,0-0.16c0,0,0-0.064,0-0.096    C512,181.088,504.704,138.592,490.24,113.92z M192,352V160l160,96L192,352z" fill="#747373" data-original="#000000" style="" class=""/></g></g><g xmlns="http://www.w3.org/2000/svg"></g><g xmlns="http://www.w3.org/2000/svg"></g><g xmlns="http://www.w3.org/2000/svg"></g><g xmlns="http://www.w3.org/2000/svg"></g><g xmlns="http://www.w3.org/2000/svg"></g><g xmlns="http://www.w3.org/2000/svg"></g><g xmlns="http://www.w3.org/2000/svg"></g><g xmlns="http://www.w3.org/2000/svg"></g><g xmlns="http://www.w3.org/2000/svg"></g><g xmlns="http://www.w3.org/2000/svg"></g><g xmlns="http://www.w3.org/2000/svg"></g><g xmlns="http://www.w3.org/2000/svg"></g><g xmlns="http://www.w3.org/2000/svg"></g><g xmlns="http://www.w3.org/2000/svg"></g><g xmlns="http://www.w3.org/2000/svg"></g></g></svg>',

		'support_icon' =>
			'<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="51" height="58" viewBox="0 0 51 58">
			<image id="support" width="51" height="58" xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAADMAAAA6CAYAAAAdrmHiAAAIbklEQVRogdWaC5BXVR3HP7suW4BjsIK1QIQShgOYYb5BQ3TM8ZmDKGZlRQ97mIOgJK3S5KBOrwmbdCoKQSPUZsxSw0xDAS1TQE3CgqIXxYqwG8hTaE7zOc3hzv+/e+/9/7eZvjM793/vueec3/ee3/m9zja0t7fTQxgMHAkMAPoC24FXgLXA33tiyqY6jtUITACmAO+VTDX8BXgEuBv4JbC/HgLUa2UuBr4IHFWhrRPYBbwBOKRC+2+B2cB9tQpRK5mhwHxXJGKVgj2moJ1JWyAzCjgdmAwcnbQ9CnwY+GtZYWohEwj8COjv/eNAG7C8wBjjgJuA07zfDFwEPFFGoLJkzvfr9wI6gE+r/wHHAZOAU4DhQO+k3w5gnYRD/2d8/gHgW8DBwE7V9qf/CzLj3LxByPXAOcDvgOOBr0kiLwKpacCv3W8PAcMkPRF4qifJHOaeaAU2SCzo+PXAl7Ro2PYz4AVgE7AHOAg4FHgXcF5i7fapnnOAIcDTtv0NOEZz3iNkgipd5pc7CVgN3AZ8xvawUrOAxd2Y2wbgAuArqmLAN4HPSvYprd9dqmAuNOZ9EThRIrgSqyURidwPjAV+mMNv7Pf9Y7ySjLUS+LzPLnfeXCiyMg/rDNdoUt8GvAi80bagOq8X+DgRQf0eBM5y849WTcPY73AfnZNnoLwr81YnC7gZ2AvcIJFXgSsSIkGFjgDOSExuVwj93g+0O14Yd6/zBJztXqobmckKGRzgPUALcKltt7jJA+GvunGD+f25oco8v35X2JwIH8ZtUV07nffiepKJHv4hQ5PwtZq1Ut8DpgMva2ZbM30/omUb0M0cC7VsYdyznedh2ybWk8xYr8u8Huc16PXXgS8nKnez7X11hKhyawxCG6rM8Yr+Jh3/ycz8NZPpk3zttV6jDh+dmM57gRFaut8ArxkZfMyNHVbmB/qR91VRvT9lxo/ztSpHl8hjzQYnwd9GYLfOMw1T5il0NZM8wlU6I3m20ZDmEcOafwLfAK7Sj21S5eKHHOJ+rIo8ZEJ48ccu2sOXPtX90x3OcuVOrfDeq36MECVUwuHJylVEEacZMMMw/TnvtwCX5CQSsERzPcbwZVWymi0JkWedZ0YR4YqSuc/85QXvg4r8ueAYaDhmGbr0A8YDU4GXkvb5RRO2omQitnrt3/2r3aJTKznPdDpga5mBypLp8FoPMila/N2Z7/UDUZbMZq9vLtm/GiKZUhljWTKxVJT19rVikP039hSZncnvKHz0O73quDqp74rjpx9rb3cD5Kmb/cPAMSRRPzZx6pu0D9Xh1YqhSf+bLBqe5P2G7hxmXjIBn5LIQIsZKY5MChO1YHjSNw0sd3QTXRQmE/zJSIPF3ob3wfG9BXh7hfd7S3qEc+yyhvZgFwlcLCAGTVjs762mzn/II2SR8uwG908QdJHhxyU6vhQjJVspoVphtvqvCm2jvS4Fri4g139R1JrF2GhqsiFDjv6e5O+7EtljpLDaUlTAyWaSlRBLVCHWK4Wi1ZnLTaLy4IOZd+dafXkeeGem//ikihlWelUZMkVXJujvR1W57jDSCKGfhuMI33+tQr8YUD5PSSJlyGCaPMyM8UPJ83N99n3vr3dfbTE3iRWWuzPjnW9lB7PW0qj1FCB8jF8B71bwcUYHCxMBI163YDgtMbNj3PD93Ssn13JWU4/zmeHmH29yBS6yhjxE1WrUGLyciblOM8QfYKx3gs65NMrGZinWJWWiw9zId1jgeMJy0/KEyEgt3mNJxeaWWolQp5VpNJmqdGq2RvO8Q/80pov3RltqKo16kLlUJ4p+aFiBvqGA8Tl/T7HwVxq1qllj4gSXeEbzYtIeTO2dFscXZGoFV+npl3h/Q63ylO08SAe4LFGb2e6L0xNCow1tJnkq1svnX9CyxX44zjLHHUQJFFGzVoWabOiRViZ36xhjujvQDR73wQrNdsCtwMyk7yF+hObk2X6Nxj1avFzJWp6VCSbzAYsNcxWqwXzjXmO05uTshswKNSZE7kjOXiIus/9exwvjNthnrvM+oBylyfRT35/WAR7kRItdoYGuUjwsmprpHwj9IvNseQWnGPvd73hh3EnOs915z1OOO5WrIqqp2ShPe6Nl+r2hxgInSDFBlcLMMEa9MxP/s1O/s88ANIY0JyaHsBOTcSL6GjJdbW6EFvNc86MDUGlljlfHh2l9bpTc7RWIoFOMBe5ZXq9MiDxqAhdVboGHS+n7a/0/AjQU1xo9b7dGPUo59ijXCuU8ANmVOdxYa6BHDBfm/CeFK5IAc45BJk56ppFy1ijM0aphKXa+vz/h3lqe7LWIU1THAarxCWkdPCXT7JKP9YuM97A0D5pMwNI8fqUq2JE8SwlFrDPEicleOCudqwNd5FHGha7wJlfsSVXwOVU7WNMD1Oy65FBnSgEiKMiNyf1LVvw7Mu9FK5fq++xMGSnkShckUcW17rFn/QgrlQ/lvS52jGRaE9sf1OUnBYhELNLjo+nOEonoSOZd7QFUV3jc2G6Ix4mDlS+q9cxYX4uDTnM5g9O7pgQR3Aef9PdRGceYYmYSNVyZI7hc6irvkMhCP9Y1yttH+f9DptlD1IBvmxmWRdhz37FvW4WzyLE+x/fy/m9M2CMf9/cEjxG3KC/K39yofY8F69trIBIxXV/QpOrFkmssUTXZPr3guHclQWmM3aK8Qf6JjZpO9APr60Cm00PbfVY740q0eb/P9jLHFpM8Qoyn2OuToPbMQOZYb5bWgUjEsiQqnmGYEiswtyVH8EWxTZVL91mU+9jGJExYW3KCamiz2N1knNXkfVu54aoiyj0iOM1dGoH2KjWtWnCo4UnEtuSgql7oozPe3eQELT7oaRycIVdPbAtkwoaK/97x/wt45t96TDaJe6E4cwAAAABJRU5ErkJggg=="/>
		  </svg>',

		'location_icon' =>
		  	'<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="45" height="55" viewBox="0 0 45 55">
			  <image id="place" width="45" height="55" xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAAC0AAAA3CAYAAACYV8NnAAAFdUlEQVRogb2afWiXVRTHv5ubzWmOsDR7EctyZVv2Yi+KEQVTS0mSleuvrMioCKIoiqTyHw0hKCGU/hERjTIQxUqMIplGQ6pRQ2W6oEJ6mZtvudnm/MaB+9DD4Tz33uf5/X4euLD7u+fl87vP/d17zn1WRRJlknEAJgFoAFAHYDSA8wD+BTAAoNe1kVLD1ZRgK2DzASwGsADA5AibcwD2A9gOYBuA7kKRZaZztktJvkXyb5Yuu0nOJ1mVhyEPcA3Jl0ieKgOslj0kZ8SyxK7pJgBbADR7dHoAdALoAtAH4DSAWrfGpzrbWQDqPUtnNYCVwXUf8c0eJTmQMUM9JF8heX3kLNWRXEhyC8nhDJ9fkbyklOXxrAe2lWR1gd9E0q4iuZ7kecP/zyQvLwK93HAmAVa7GSsKq9sskgcywCfkgW4hOaKcHHePtlyw6TaO5FYD/BuStTHQV5I8pox7STZXCDhpsimsM8DXxEB/oYwGSd5WYeA0+GZjSc71Qbca3/TxnIFlP7/OfdGZoZ3AaPUkfzLWd40FLTvBYaW8OTKQ2D5C8rOM7fGIe8xTIv3dSHIoa/LSig8rpZMkJ0cEuMvNRIwMO/iLIvy+Y8x2lYZuV0pvRjh+wnNI+KSD5GUB37KjnFA+5qWhp6jBQZcY+Zw+lgElW+X3JHe4hCgrsfrRgeWZ7Q1p6BfUYGgtN5I8o2yGXJCJxnpvcV9Ey4ZAnGlKv0/8JYM71WBrwNnnSl8OnjkBm9EkNxrgdwfs9E7SnAwcVQMNHidNRuCWyF1hFMmvle22gM0aPaHVriy6IpX4/QLgpCcxXKr6WwF86U0l/xdJOZ93ZVgiC12pliWd6vNGgR6jPjwYCHyP6n8YCZz2vzfVl5z7To/+IdWfWO2M0tIXCDpN9ffF82baTPfoHlP98Rb08UDAi1N/S5U9GAGppVf16zy6p1R/bLUrc9LSEAiYdlLvKZ98MkmNDXh09XofEOjhnNA9qq/XeIzMVTpHPDbjVf+MQJ9VHzYGgrar/vKcwFLgzkn1hwB859G/QfV7E+j+1IeNgTX2keovAbAoEli21w8AVKU+2x5YHjNV/3CygesN/4HAhr9N6ctdyP0BG6krPzYOpjsCdvu0fjLwuhpYF3B0LcnTykayvbUkr1a6UuMtJtllAK8PxJmkqvV/JB1IBmcoZ/2ugvA5XGIUv4kcJLmL5LcuL7FE0tMxgRivKrtPqfJpnZi8FpFLtJE8mwHlk70RZZikrX8qH0s19NNKQR7FNRHgt5DcHwkrefpK61rAaO8p298TO732dI3YEVkaSRm0yD2+fgNWnuLbkeWbtIcMH08l41p5nqG8Ke9VrINrdnd8od+GbjcbN7Pt6Ss4y2itAb4iJ3TRJvd3v6nYJ1wFAx90rbuO0tJWYeB647cxYl3FZTmQi79u5UB2idkVAq5yvwctL1r6PkfTjR+VVNZTKwCtq276DriQs3uNm54DgRoyb3vSAN7l6slC0NKWGU53p+/WSmj3GZPSFZqU2ICrDPBQ3hBqjcYR/1fM8svzQ/nEAH+5IPAEdymZlkF3Lxi0zxOo3p2QaRlxp1ceP6ONe0O6F1JRPvLOkKSKv6pgcj12a44ntskAfiMPR5FH22Qcs0fda4+Q7QoDeGNehiLQ0hYYufQPJMd6bNoM4D1uuVwQaGnPGRA7MvbX2Ube3Z31yq2S0NLeN8DfVTqyhek76n534haKWyr0KOOaWOQZN95gvNgccidt4bilQsOVRZ0K7BzJB93JqWVZqTHLAQ1Xgf+h6Kx33qvKEa9c0NJu9/y3At2JmrcCqjg03Gs9a4Y7CpRdFwwaxl2FnKBykpYtRiWgpcl7Elkqh0jeVFbfJP4DpEGom8hghk4AAAAASUVORK5CYII="/>
			</svg>',
		'menu_icon' =>
			'<?xml version="1.0"?>
			<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svgjs="http://svgjs.com/svgjs" version="1.1" width="512" height="512" x="0" y="0" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512" xml:space="preserve" class=""><g>
			<g xmlns="http://www.w3.org/2000/svg"><path style="" d="M479.18,91.897H32.821C14.69,91.897,0,77.207,0,59.077s14.69-32.821,32.821-32.821H479.18   c18.13,0,32.82,14.69,32.82,32.821S497.31,91.897,479.18,91.897z" fill="#ffffff" data-original="#ff485a" class=""/><path style="" d="M295.385,288.821H32.821C14.69,288.821,0,274.13,0,256s14.69-32.821,32.821-32.821h262.564   c18.13,0,32.821,14.69,32.821,32.821S313.515,288.821,295.385,288.821z" fill="#ffffff" data-original="#ff485a" class=""/></g><path xmlns="http://www.w3.org/2000/svg" style="" d="M479.18,288.821h-52.513c-18.13,0-32.821-14.69-32.821-32.821s14.69-32.821,32.821-32.821h52.513  c18.13,0,32.82,14.69,32.82,32.821S497.31,288.821,479.18,288.821z" fill="#ffffff" data-original="#ffbbc0" class=""/><path xmlns="http://www.w3.org/2000/svg" style="" d="M479.18,485.744H32.821C14.69,485.744,0,471.053,0,452.923c0-18.13,14.69-32.821,32.821-32.821  H479.18c18.13,0,32.82,14.69,32.82,32.821C512,471.053,497.31,485.744,479.18,485.744z" fill="#ffffff" data-original="#ff485a" class=""/><g xmlns="http://www.w3.org/2000/svg"></g><g xmlns="http://www.w3.org/2000/svg"></g><g xmlns="http://www.w3.org/2000/svg"></g><g xmlns="http://www.w3.org/2000/svg"></g><g xmlns="http://www.w3.org/2000/svg"></g><g xmlns="http://www.w3.org/2000/svg"></g><g xmlns="http://www.w3.org/2000/svg"></g><g xmlns="http://www.w3.org/2000/svg"></g><g xmlns="http://www.w3.org/2000/svg"></g><g xmlns="http://www.w3.org/2000/svg"></g><g xmlns="http://www.w3.org/2000/svg"></g><g xmlns="http://www.w3.org/2000/svg"></g><g xmlns="http://www.w3.org/2000/svg"></g><g xmlns="http://www.w3.org/2000/svg"></g><g xmlns="http://www.w3.org/2000/svg"></g></g></svg>
			',
		'remote_icon' =>
			'<?xml version="1.0"?>
			<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svgjs="http://svgjs.com/svgjs" version="1.1" width="512" height="512" x="0" y="0" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512" xml:space="preserve" class=""><g><g xmlns="http://www.w3.org/2000/svg" id="Outline"><path d="M181.54,47.678,170.46,36.135a217.8,217.8,0,0,1,16.613-14.4l9.854,12.606A201.731,201.731,0,0,0,181.54,47.678ZM496,264a23.972,23.972,0,0,1-28.235,23.618c-2.966,4.573-6.151,9.042-9.53,13.332A175.147,175.147,0,0,1,320,368h-8V262.624a24,24,0,0,1,0-45.248V200H246.624a24.12,24.12,0,0,1-13.778,14.3c.615,8.263,1.566,16.448,2.864,24.41l-15.792,2.574c-1.4-8.6-2.417-17.452-3.065-26.374A24.111,24.111,0,0,1,201.376,200H160.221a159.8,159.8,0,0,0,6,36.346l-15.376,4.425a176.987,176.987,0,0,1-4.563-77.094,23.983,23.983,0,0,1,13.08-43.661,174.92,174.92,0,0,1,10.7-20.215A176,176,0,0,1,320,16a175.106,175.106,0,0,1,93.1,26.627,23.987,23.987,0,0,1,34.048,27.684A176.221,176.221,0,0,1,487.616,245.8,23.95,23.95,0,0,1,496,264ZM458.574,112H405.238a279.253,279.253,0,0,1,9.921,57.091A24.109,24.109,0,0,1,430.624,184h49.154A159.4,159.4,0,0,0,458.574,112ZM408,184a8,8,0,1,0,8,8A8.009,8.009,0,0,0,408,184Zm8-120a8,8,0,1,0,8-8A8.009,8.009,0,0,0,416,64ZM365.343,38.53C379.23,52.254,391.181,71.815,400.03,96h47.976q-4.816-6.411-10.28-12.329a23.99,23.99,0,0,1-35.794-29.1A158.689,158.689,0,0,0,365.343,38.53ZM328,96h54.9C369.421,62.155,349.663,39.78,328,33.7Zm0,88h57.376a24.12,24.12,0,0,1,13.78-14.3A264.889,264.889,0,0,0,388.538,112H328Zm0,33.376a24,24,0,0,1,0,45.248V288h41.376a24.07,24.07,0,0,1,19.084-15.737,264.59,264.59,0,0,0,10.7-57.958A24.12,24.12,0,0,1,385.376,200H328ZM392,288a8,8,0,1,0,8,8A8.009,8.009,0,0,0,392,288ZM254.1,84.6A24.1,24.1,0,0,1,262.624,96H312V32.2l-.146.006-.022,0C289.924,34.322,269.145,53.284,254.1,84.6ZM191.989,96h25.387a24.048,24.048,0,0,1,21.185-15.953c6.908-14.879,15.1-27.479,24.208-37.464A160.2,160.2,0,0,0,191.989,96ZM160,152a8,8,0,1,0-8-8A8.009,8.009,0,0,0,160,152Zm56.845,17.089a287.241,287.241,0,0,1,7.282-47.109,24.086,24.086,0,0,1-6.751-9.98H181.417q-3.675,6.352-6.755,13.019a23.981,23.981,0,0,1-12.854,42.906c-.8,5.316-1.341,10.687-1.608,16.075h41.176A24.114,24.114,0,0,1,216.845,169.089ZM232,192a8,8,0,1,0-8,8A8.009,8.009,0,0,0,232,192Zm0-88a8,8,0,1,0,8-8A8.009,8.009,0,0,0,232,104Zm80,80V112H262.624A24.039,24.039,0,0,1,240,128c-.289,0-.576-.012-.862-.022a272.045,272.045,0,0,0-6.3,41.715A24.124,24.124,0,0,1,246.624,184Zm16,56a8,8,0,1,0-8,8A8.009,8.009,0,0,0,328,240Zm44.4,69.83a23.933,23.933,0,0,1-3.028-5.83H328v46.463C344.371,345.832,359.8,331.762,372.4,309.83ZM434.269,304H414.624a23.976,23.976,0,0,1-29.107,15.1,130.005,130.005,0,0,1-20.031,26.361A158.879,158.879,0,0,0,434.269,304ZM472,240c.225,0,.448.011.672.017A159.292,159.292,0,0,0,479.8,200H430.624a24.11,24.11,0,0,1-15.464,14.909,277.056,277.056,0,0,1-10.913,60.462A24.129,24.129,0,0,1,414.624,288h33.365q3.059-4.077,5.852-8.334A23.981,23.981,0,0,1,472,240Zm8,24a8,8,0,1,0-8,8A8.009,8.009,0,0,0,480,264ZM56,280a8.009,8.009,0,0,1,8-8H272a8.009,8.009,0,0,1,8,8V424h16V280a24.027,24.027,0,0,0-24-24H64a24.027,24.027,0,0,0-24,24v16H56ZM320,448v24a24.027,24.027,0,0,1-24,24H40a24.027,24.027,0,0,1-24-24V448a8,8,0,0,1,8-8H40V312H56V440h80a8,8,0,0,1,5.657,2.343L147.313,448h41.374l5.656-5.657A8,8,0,0,1,200,440H312A8,8,0,0,1,320,448Zm-16,8H203.313l-5.656,5.657A8,8,0,0,1,192,464H144a8,8,0,0,1-5.657-2.343L132.687,456H32v16a8.009,8.009,0,0,0,8,8H296a8.009,8.009,0,0,0,8-8ZM125.352,238.161A200.42,200.42,0,0,1,169.4,60.391L157.358,49.857A216.424,216.424,0,0,0,109.78,241.839ZM320,392c-2.558,0-5.147-.049-7.7-.146l-.606,15.989c2.751.1,5.545.157,8.3.157a214.825,214.825,0,0,0,170.266-83.072l-12.606-9.856A198.917,198.917,0,0,1,320,392ZM208,360a24,24,0,0,1,0,48H128a24,24,0,0,1,0-48Zm0,16H128a8,8,0,0,0,0,16h80a8,8,0,0,0,0-16ZM72,288v16H88V288Zm32,0v16h16V288Zm32,0v16h16V288Zm128,32H72v16H264Z" fill="#ffffff" data-original="#000000" style="" class=""/></g></g></svg>
			',
	);

	return $svg_content[$key];
}
