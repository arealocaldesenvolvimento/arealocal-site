/**
 * Funções genéricas do tema neste arquivo
 */
window.onload = () =>{

	// Verifica o scroll ao carregar as páginas
	verifyScroll();

	/*
	* Insere os itens do menu desktop no menu mobile
	*/
	let suporte_menu = document.querySelector('#main-menu > .suporte-menu').cloneNode(true);
	let sociais_menu = document.querySelector('#main-menu > .sociais-menu').cloneNode(true);
	document.querySelector('#responsive-menu-container').append(suporte_menu, sociais_menu);

	/*
	Pega o scroll e aplica classe no header [DESKTOP]
	 */
	window.onscroll = () => {
		verifyScroll();
	}

	/*
	* Abre ou fecha o menu [DESKTOP]
	*/
	document.getElementById('open-menu').addEventListener('click', function(){
		let button_menu = this;

		if(button_menu.classList.contains('menu-open')){
			document.getElementById('menu-desktop').style.width = '0%';
			document.querySelector('header[role=heading] .container .orcamento-menu > div > a:first-of-type').style.visibility = 'visible';
			document.querySelector('header[role=heading] .container .orcamento-menu > div > a:first-of-type').style.opacity = '1';
			document.querySelector('.container h1#wrap-logo').style.visibility = '1';
			document.querySelector('.container h1#wrap-logo').style.transform = 'translateX(0)';

			button_menu.classList.remove('menu-open');
		} else {
			document.getElementById('menu-desktop').style.width = '100%';
			document.querySelector('header[role=heading] .container .orcamento-menu > div > a:first-of-type').style.opacity = '0';
			document.querySelector('header[role=heading] .container .orcamento-menu > div > a:first-of-type').style.visibility = 'hidden';
			document.querySelector('.container h1#wrap-logo').style.transform = 'translateX(-100vw)';
			document.querySelector('.container h1#wrap-logo').style.visibility = '0';

			button_menu.classList.add('menu-open');
		}
	});

	/*
	* Fecha o menu ao clicar em um link
	*/
	let links = document.querySelectorAll('.menu-item a');
	for(let link of links){
		let button_menu = document.getElementById('open-menu');
		link.addEventListener('click', function(){
			console.log('Teste');

			document.getElementById('menu-desktop').style.width = '0%';
			document.querySelector('header[role=heading] .container .orcamento-menu > div > a:first-of-type').style.visibility = 'visible';
			document.querySelector('header[role=heading] .container .orcamento-menu > div > a:first-of-type').style.opacity = '1';
			document.querySelector('.container h1#wrap-logo').style.visibility = '1';
			document.querySelector('.container h1#wrap-logo').style.transform = 'translateX(0)';

			button_menu.classList.remove('menu-open');
		});
	}

	/*
	Slider das imagens da trajetória na page sobre nós
	*/
	if(document.querySelector('section.page-sobre-nos')){
		var slider_img = tns({
			container: '#galeria-trajetoria',
			items: 1,
			nav: false,
			prevButton: '#slide-controls button.prev',
			nextButton: '#slide-controls button.next',
		});

		var slider_img = tns({
			container: '#content-trajetoria',
			items: 1,
			nav: false,
			prevButton: '#slide-controls button.prev',
			nextButton: '#slide-controls button.next',
		});

		AOS.init();
	}

	/*
	* AOS na página do blog
	*/
	if(document.querySelector('.categorias-destaque')){
		AOS.init();
	}

}

function verifyScroll(){
	if (document.body.scrollTop > 1 || document.documentElement.scrollTop > 1) {
		document.querySelector('header[role=heading]').classList.add('header-scroll');
		document.querySelector('#responsive-menu-button').classList.add('header-scroll');
	} else {
		document.querySelector('header[role=heading]').classList.remove('header-scroll');
		document.querySelector('#responsive-menu-button').classList.remove('header-scroll');
	}
}
