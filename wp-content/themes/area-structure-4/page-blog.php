<?php get_header()?>

<?php
$destaques = new WP_Query(array(
    'post_type' => 'post',
    'post_status' => 'publish',
	'posts_per_page' => 4,
	'category_name' => 'destaque',
    'order' => 'DESC'
));

$categoria_filtro = get_category(get_queried_object_id())->slug;
$paged = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;

$blog = new WP_Query(array(
    'post_type' => 'post',
    'posts_per_page' => 8,
    'posts_join' => true,
    'category_name' => $categoria_filtro,
    'orderby' => 'date',
    'order' => 'desc',
    'paged'  => $paged
));

$pg_blog = get_page_by_path('blog', OBJECT, 'page');
?>

<section class="page-blog">
	<div class="page-header">
		<div class="container">
			<div class="title">
				<h1><?= get_the_title($pg_blog->ID) ?></h1>
				<p>
					Lorem ipsum dolor sit amet, consectetur <br>
					adipiscing elit. Suspendisse imperdiet, magna vel <br>
					rhoncus vulputate, lacus mauris ullamcorper est, <br>
					id tincidunt diam leo euismod arcu.
				</p>
			</div>
			<div class="thumb">
				<?= get_the_post_thumbnail($pg_blog->ID, 'full') ?>
			</div>
		</div>
	</div>

	<div class="content">
		<div class="container">
			<div class="title">
				<span>confira os</span>
				<h2>destaques</h2>
				<h3>Lorem ipsum in dolor <br>sit amet, consectetur</h3>
			</div>

			<div class="destaques-blog">
				<?php wp_reset_postdata(); ?>

				<?php if ($destaques->have_posts()) :
					while ($destaques->have_posts()): $destaques->the_post(); ?>

					<article class="card">
						<a href="<?= get_the_permalink(get_the_ID()) ?>">
							<div class="thumbnail">
								<img src="<?= get_thumbnail_url(get_the_ID(), 'medium') ?>" alt="<?= get_the_title(get_the_ID()) ?>" />
							</div>
							<div class="info">
								<h3><?= get_the_title(get_the_ID()) ?></h3>
								<span><?= get_the_date('d/m/Y') ?></span>
								<!-- <span><?= get_the_date('d de F de Y') ?></span> -->
								<p><?= the_excerpt(get_the_ID()) ?></p>
							</div>
						</a>
					</article>

					<?php endwhile ?>
					<div style="clear: both;"></div>
				<?php endif ?>
				<?php wp_reset_postdata(); ?>
			</div>

			<div class="title title-categorias">
				<span>Navegue <br>pelas</span>
				<h2>categorias</h2>
			</div>

			<div class="categorias-destaque">
				<ul>
					<?php
					$categorias = get_categories(array(
						'hide_empty' => false,
						'orderby' => slug,
						'order' => 'DESC'
					));
					foreach ($categorias as $categoria) :
						if (get_field('categoria_destaque', 'category_'.$categoria->term_id)): ?>
							<li style="background-image: url(<?= get_field('imagem', 'category_'.$categoria->term_id) ?>)" data-aos="fade-up">
								<a href="<?= get_category_link($categoria->term_id) ?>">
									<span><?= $categoria->name ?></span>
								</a>
							</li>
					<?php endif; endforeach; ?>
				</ul>
			</div>

			<div class="title title-categorias">
				<span>Confira <br>todas as</span>
				<h2>matérias</h2>
			</div>

			<div class="todas-materias">
				<div class="articles">
					<?php if ($blog->have_posts()) :
						while ($blog->have_posts()): $blog->the_post(); ?>

						<article class="card" data-aos="fade-up">
							<a href="<?= get_the_permalink(get_the_ID()) ?>">
								<div class="thumbnail">
									<img src="<?= get_thumbnail_url(get_the_ID(), 'medium') ?>" alt="<?= get_the_title(get_the_ID()) ?>" />
								</div>
								<div class="info">
									<h3><?= get_the_title(get_the_ID()) ?></h3>
									<span><?= get_the_date('d/m/Y') ?></span>
									<!-- <span><?= get_the_date('d de F de Y') ?></span> -->
									<?= the_excerpt(get_the_ID()) ?>
									<div class="veja-mais">
										<button type="button">VEJA MAIS</button>
									</div>
								</div>
							</a>
						</article>

						<?php endwhile ?>
						<div style="clear: both;"></div>
					<?php else: ?>
						<p>Nehuma notícia encontrada</p>
					<?php endif ?>
				</div>
				<div class="pagination">
					<nav>
						<?php paginationLinks($blog) ?>
					</nav>
				</div>
			</div>
		</div>
	</div>

</section>

<?php get_footer()?>
