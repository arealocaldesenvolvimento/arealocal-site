<?php get_header()?>
<?php while (have_posts()) : the_post(); ?>

<section class="page-sobre-nos">
	<div class="page-header">
		<div class="container">
			<div class="title">
				<h1><?= the_title() ?></h1>
				<p><?= the_content() ?></p>
			</div>
			<div class="thumb">
				<?= get_the_post_thumbnail(get_the_ID(), 'full') ?>
			</div>
		</div>
	</div>

	<div class="content">
		<div class="container">

			<div class="especialista">
				<div>
					<h2><span>somos especialistas em </span>tecnologia web!</h2>
				</div>
				<div>
					<p>Temos mais de 15 anos de experiência e atuamos em todas as regiões do estado de Santa Catarina. Nosso objetivo é, com criatividade, originalidade e planejamento, oferecer soluções personalizadas para web. Dessa forma, fazemos da internet uma forte aliada de nossos clientes.</p>
				</div>
			</div>
		</div>
	</div>

	<div class="trajetoria" id="trajetoria">
		<div class="title-mobile">
			<span>Acompanhe <br>nossa</span>
			<h2>Trajetória</h2>
		</div>
		<div>
			<div>
				<ul id="galeria-trajetoria">
				<?php if( have_rows('trejetoria') ):
						while ( have_rows('trejetoria') ) : the_row();?>
						<li>
							<img src="<?= get_sub_field('imagem') ?>" alt="<?= the_sub_field('titulo') ?>">
						</li>

					<?php endwhile; ?>
				<?php endif;?>
				</ul>
			</div>
		</div>
		<div>
			<div class="bg-absolute"></div>
			<div>
				<div class="title">
					<span>Acompanhe <br>nossa</span>
					<h2>Trajetória</h2>
				</div>
				<div id="slide-controls">
					<button type="button" class='prev'><img src="<?= image_url('arrow.png') ?>" alt="Próximo"></button>
					<button type="button" class='next'><img src="<?= image_url('arrow.png') ?>" alt="Anterior"></button>
				</div>
				<ul id="content-trajetoria">
				<?php if( have_rows('trejetoria') ):
						while ( have_rows('trejetoria') ) : the_row();?>

						<li>
							<div>
								<div class="left">
									<h3><?= the_sub_field('titulo') ?></h3>
									<p><?= the_sub_field('texto') ?></p>
								</div>
								<div class="right">
									<p><?= the_sub_field('ano') ?></p>
								</div>
							</div>
						</li>

					<?php endwhile; ?>
				<?php endif;?>
				</ul>
			</div>
		</div>
	</div>

	<div class="equipe" id="equipe">
		<div class="container">
			<div class="line">
				<div data-aos="fade-up">
					<div class="title">
						<span>conheça a <br>equipe</span>
						<h2>show <br>all</h2>
						<h3>O time por <br>trás dos projetos!</h3>
					</div>
				</div>
				<?php $aux = 1; ?>
				<?php if( have_rows('equipe') ):
					while ( have_rows('equipe') ) : the_row();?>
						<?php
						if($aux == 3){
							echo '</div>';
							echo '<div class="line">';
						} else if($aux != 3 && $aux % 3 == 0){
							echo '</div>';
							echo '<div class="line">';
						}
						?>
						<div data-aos="fade-up">
							<img src="<?= the_sub_field('foto') ?>" alt="<?= the_sub_field('nome') ?>">
							<div class="info">
								<h4><?= the_sub_field('nome') ?></h4>
								<span><?= the_sub_field('cargo') ?></span>
							</div>
						</div>

						<?php $aux++; ?>
					<?php endwhile; ?>
				<?php endif;?>
			</div>
		</div>
	</div>
</section>

<?php  endwhile; ?>
<?php get_footer()?>
