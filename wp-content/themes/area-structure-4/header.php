<!DOCTYPE html>
<html <?php language_attributes() ?> xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="X-UA-Compatible" content="IE=7">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="web_author" content="Área Local">
    <meta name="theme-color" content="#0057a8">
    <link rel="icon" href="<?php image_url('favicon.png') ?>">
    <link rel="pingback" href="<?php bloginfo('pingback_url') ?>">
	<link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" media="all" src="<?= get_template_directory_uri() ?>/public/index.css">
    <link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo('stylesheet_url') ?>" >
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/css/all.min.css" integrity="sha512-1PKOgIY59xJ8Co8+NE6FZ+LOAZKjy+KY8iq0G4B3CyeY6wYHN3yt9PW0XpSriVlkMXe40PTKnXrLnZ9+fkDaog==" crossorigin="anonymous" />
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/tiny-slider/2.9.3/tiny-slider.css">
    <title>
        <?= is_front_page() || is_home() ? get_bloginfo('name') : wp_title('') ?>
    </title>
    <?php wp_head() ?>
</head>
<body <?php body_class() ?>>

<?= do_shortcode('[responsive_menu]') ?>

    <!-- Header -->
    <header role="heading">
		<div class="mobile-menu">
			<?= get_logo() ?>
		</div>
		<div class="container">
			<?= get_logo() ?>

			<div class="orcamento-menu">
				<div>
					<a href="arealocal.com.br"><i class="far fa-file-alt"></i> Orçamento</a>
					<button type="button" id="open-menu">
						<span></span>
						<span></span>
						<span></span>
					</button>
				</div>
			</div>
		</div>
	</header>

	<section id="menu-desktop">
		<nav id="main-menu">
			<?= get_logo() ?>

			<?php wp_nav_menu(array('theme_location' => 'principal', 'container' => false)); ?>

			<div class="suporte-menu">
				<a href="https://api.whatsapp.com/send?phone=554735219850" target="_blank"><i class="fab fa-whatsapp"></i> Whatsapp</a>
				<a href="https://suporte.arealocal.com.br" target="_blank">Suporte</a>
				<a href="https://suporte.arealocal.com.br/remoto" target="_blank">Acesso Remoto</a>
			</div>
			<div class="sociais-menu">
				<a href="https://www.facebook.com/arealocal?fref=ts" title="Curta nossa página" alt="facebook" class="facebook" target="_blank"><?= svg('facebook_icon'); ?></a>
				<a href="https://instagram.com/arealocal" title="Nossas fotos no Instagram" alt="instagram" class="instagram" target="_blank"><?= svg('instagram_icon'); ?></a>
				<a href="https://www.linkedin.com/company/2592225?trk=vsrp_companies_res_name&trkInfo=VSRPsearchId%3A4053558421424728142768%2CVSRPtargetId%3A2592225%2CVSRPcmpt%3Aprimary" title="Conecte-se a nós" alt="linkedin" class="linkedin" target="_blank"><?= svg('linkedin_icon'); ?></a>
				<a href="https://www.youtube.com/user/arealocal01" title="Nosso canal do Youtube" alt="youtube" class="youtube" target="_blank"><?= svg('youtube_icon'); ?></a>
			</div>
		</nav>
	</section>
    <!-- Wrapper -->
    <div id="wrapper">
