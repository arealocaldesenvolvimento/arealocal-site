<?php get_header() ?>
<?php while (have_posts()) : the_post(); ?>

	<section class="page-fale-conosco">
		<div class="page-header">
			<div class="container">
				<div class="title">
					<h1><?= the_title() ?></h1>
					<p><?= the_content() ?></p>
				</div>
				<div class="thumb">
					<?= get_the_post_thumbnail(get_the_ID(), 'full') ?>
				</div>
			</div>
		</div>

		<div class="content">
			<div class="container">
				<div>
					<div>
						<h3>Suporte<span>.</span></h3>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras varius turpis sed enim dapibus accumsan.</p>
						<a href="https://suporte.arealocal.com.br" target="_blank">Solicitar</a>
					</div>
					<div>
						<h3>Atendimento<span>.</span></h3>
						<p>Experiemente um atendimento persolnalizado e eficiente. Chama a gente!</p>
						<a href="https://api.whatsapp.com/send?phone=554735219850" target="_blank"><i class="fab fa-whatsapp"></i> (47) 3521-9850</a>
					</div>
					<div>
						<h3>Trabalhe Conosco<span>.</span></h3>
						<p>A Área Local é como sua segunda casa, o trabalho é árduo e o ambiente é descontraído.</p>
						<a href="#">Entrar no time!</a>
					</div>
				</div>

				<div class="contato">
					<div>
						<div class="title">
							<span>curtiu nossos <br>serviços?</span>
							<h2>Entre em <br>contato</h2>
							<h3>Entre em contato para <br>podermos colocar você <br>no caminho do sucesso.</h3>
						</div>
						<p>Preencha o formulário e envie <br>sua mensagem.</p>
						<p>Estamos sempre prontos para ajudar sua <br>empresa a gerar resultados.</p>
					</div>
					<div>
						<?= do_shortcode('[contact-form-7 id="6" title="Contato"]') ?>
					</div>
				</div>
			</div>
			<div class="maps">
				<div class="local">
					<h3>Vem tomar um <br>café com a gente! <img src="<?= image_url('coffecup.png') ?>"><img src="<?= image_url('emote.png') ?>"></h3>
					<p><a href="https://g.page/arealocal?share" target="_blank">R. Antônio Karam, 31 - Sumaré, Rio do Sul <br>Santa Catarina, 89165-543</a></p>
				</div>
				<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d14191.031517369782!2d-49.6510278!3d-27.2267505!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xe0f291172718d49f!2s%C3%81rea%20Local%20-%20Sites%20e%20e-commerce!5e0!3m2!1spt-BR!2sbr!4v1603463582079!5m2!1spt-BR!2sbr" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0" loading="lazy"></iframe>
			</div>
		</div>
	</section>

<?php endwhile; ?>
<?php get_footer() ?>
