<?php
/**
 * As configurações básicas do WordPress
 *
 * O script de criação wp-config.php usa esse arquivo durante a instalação.
 * Você não precisa usar o site, você pode copiar este arquivo
 * para "wp-config.php" e preencher os valores.
 *
 * Este arquivo contém as seguintes configurações:
 *
 * * Configurações do MySQL
 * * Chaves secretas
 * * Prefixo do banco de dados
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar estas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define( 'DB_NAME', 'arealocal_new' );

/** Usuário do banco de dados MySQL */
define( 'DB_USER', 'root' );

/** Senha do banco de dados MySQL */
define( 'DB_PASSWORD', 'root' );

/** Nome do host do MySQL */
define( 'DB_HOST', '192.168.0.39' );

/** Charset do banco de dados a ser usado na criação das tabelas. */
define( 'DB_CHARSET', 'utf8' );

/** O tipo de Collate do banco de dados. Não altere isso se tiver dúvidas. */
define( 'DB_COLLATE', '' );

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las
 * usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org
 * secret-key service}
 * Você pode alterá-las a qualquer momento para invalidar quaisquer
 * cookies existentes. Isto irá forçar todos os
 * usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '[qRF4+G6au#^SRxz6q:fDJ!&vd?v0++*4<I2}5On^HS8X4MxWv@b10wy+{5GLP56');
define('SECURE_AUTH_KEY',  ';<FTg%43NQ21;_6T,8EoqcA{?,E ^<%E>#uZh090WlWL4-2tgV9Zm+|XK;Z!2~R}');
define('LOGGED_IN_KEY',    '*>(l%8DhTa?f1=1i}=x8#0EF }3Vcd*[.X7;^v!M?lbCY$isovE|JS*%?pxc$TqH');
define('NONCE_KEY',        ';+dWSOvYPnX#^}-+@Q8IYW+2>^|<]d10=|j}ulD++%wn^<SWhOO$23c^&-o~9|a8');
define('AUTH_SALT',        '0lT]g2f=9)?84?-ZID{<G6s;vXI7+v:<xEH.=tjMXLw9<[&3gaR.Hdq?y5>h~Y!Y');
define('SECURE_AUTH_SALT', '7)y?7=_yg5<HN;UOMY||iR1T:Z[rKOZ*W9a3BXs)m2H{C[]nS|dK|seS9bQWv=Kl');
define('LOGGED_IN_SALT',   '*ACp@t)a9eOsxh`w3jR-;z)hY>Ge,|2JhUYoqN+JsLk{ni~xZ8KDATmF%Dr]#5Kk');
define('NONCE_SALT',       'H!6l5+U+9bs0hl4`8u%,LQG[:?2,FAE_<)x*|xL&G|SrU5-0b8u8-0g|dg6 [[0M');

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der
 * um prefixo único para cada um. Somente números, letras e sublinhados!
 */
$table_prefix = 'al_';

/**
 * Para desenvolvedores: Modo de debug do WordPress.
 *
 * Altere isto para true para ativar a exibição de avisos
 * durante o desenvolvimento. É altamente recomendável que os
 * desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 *
 * Para informações sobre outras constantes que podem ser utilizadas
 * para depuração, visite o Codex.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Configura as variáveis e arquivos do WordPress. */
require_once ABSPATH . 'wp-settings.php';
